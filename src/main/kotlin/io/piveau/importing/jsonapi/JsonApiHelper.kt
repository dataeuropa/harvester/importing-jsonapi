package io.piveau.importing.jsonapi

import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.json.pointer.JsonPointer

typealias JSONAPIResponse = JsonObject
typealias JSONAPIResourceObject = JsonObject

operator fun JsonObject.component1(): JsonObject = getJsonObject("meta", JsonObject())
operator fun JsonObject.component2(): JsonObject = getJsonObject("links", JsonObject())

val JSONAPIResponse.collectionData: JsonArray
    get() = getJsonArray("data", JsonArray())

val JSONAPIResponse.singleData: JsonObject
    get() = getJsonObject("data", JsonObject())

val JSONAPIResponse.version: String
    get() = getJsonObject("jsonapi", JsonObject()).getString("version", "")

val JSONAPIResponse.failed: Boolean
    get() = containsKey("error")

val JSONAPIResponse.next: String?
    get() = getJsonObject("links")?.getString("next")

val JSONAPIResourceObject.resources: JSONAPIResourceObject
    get() = JsonPointer
        .from("/relationships/resources")
        .queryJsonOrDefault(this, JsonObject()) as JSONAPIResourceObject

val JSONAPIResourceObject.institution: JSONAPIResourceObject
    get() = JsonPointer
        .from("/relationships/institution")
        .queryJsonOrDefault(this, JsonObject()) as JSONAPIResourceObject