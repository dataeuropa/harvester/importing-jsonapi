package io.piveau.importing.jsonapi

import io.piveau.json.putIfNotEmpty
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.json.pointer.JsonPointer
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.predicate.ResponsePredicate
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.kotlin.ext.web.client.sendAwait
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

data class Dataset(val dataset: JsonObject, val dataInfo: JsonObject)
data class Page(val datasets: JsonArray, val total: Int, val page: String)

class JsonApiClient(private val client: WebClient) {

    suspend fun get(address: String): JSONAPIResponse =
        client.getAbs(address)
            .`as`(BodyCodec.jsonObject() as BodyCodec<JSONAPIResponse>)
            .expect(ResponsePredicate.SC_SUCCESS)
            .sendAwait().body()

    private suspend fun resolve(resourceObject: JSONAPIResourceObject) =
        JsonPointer.from("/links/related").queryJson(resourceObject)?.let {
            get(it.toString())
        } ?: JSONAPIResponse()

    @FlowPreview
    private fun pagesFlow(address: String): Flow<Page> = flow {
        var nextLink: String? = address
        do {
            val page = get(nextLink as String)

            nextLink = if (!page.failed) {
                val total = page.component1().getInteger("count", -1)
                val data = page.collectionData
                emit(Page(data, total, nextLink))
                page.next
            } else {
                null
            }
        } while (nextLink != null)
    }

    @FlowPreview
    fun datasetsFlow(address: String): Flow<Dataset> = flow {
        coroutineScope {
            pagesFlow(address).collect { (datasets, total, page) ->
                datasets.map { it as JSONAPIResourceObject }.forEach {

                    val resources = async { resolve(it.resources).collectionData }
                    val institution = async { resolve(it.institution).singleData }

                    val dataset = JsonObject()
                        .put("dataset", it)
                        .put("resources", resources.await())
                        .putIfNotEmpty("institution", institution.await())

                    val dataInfo = JsonObject()
                        .put("total", total)
                        .put("identifier", "${it.getString("type")}_${it.getString("id")}")
                        .put("page", page)

                    emit(Dataset(dataset, dataInfo))
                }
            }
        }
    }

}
