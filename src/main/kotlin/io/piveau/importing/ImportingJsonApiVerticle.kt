package io.piveau.importing

import io.piveau.importing.jsonapi.*
import io.piveau.pipe.PipeContext
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.coroutines.CoroutineContext

class ImportingJsonApiVerticle : CoroutineVerticle() {

    private lateinit var jsonApiClient: JsonApiClient

    override suspend fun start() {
        jsonApiClient = JsonApiClient(WebClient.create(vertx))
        vertx.eventBus().consumer<PipeContext>(ADDRESS) {
            launch(vertx.dispatcher() as CoroutineContext) {
                handlePipe(it)
            }
        }
    }

    @FlowPreview
    private suspend fun handlePipe(message: Message<PipeContext>) {
        with(message.body()) {
            val address = config.getString("address")
            val catalogue = config.getString("catalogue")

            val identifiers = mutableMapOf<String, JsonObject>()
            var count = 0
            jsonApiClient.datasetsFlow(address)
                .onCompletion {
                    when {
                        it != null -> setFailure(it)
                        else -> {
                            delay(8000)
                            val dataInfo = JsonObject()
                                .put("content", "identifierList")
                                .put("catalogue", catalogue)
                            setResult(
                                JsonArray(identifiers.keys.toList()).encodePrettily(),
                                "application/json",
                                dataInfo
                            ).forward()
                            log.info("Importing finished")
                        }
                    }
                }
                .collect { (dataset, dataInfo) ->
                    val identifier = dataInfo.getString("identifier")
                    if (identifiers.contains(identifier)) {
                        log.warn(
                            "Duplicate identifier {}: {} - {}",
                            identifier,
                            identifiers[identifier]?.encodePrettily() ?: "",
                            dataInfo.encodePrettily()
                        )
                    } else {
                        identifiers[identifier] = dataInfo
                    }
                    dataInfo.put("count", ++count)
                    dataInfo.put("catalogue", catalogue)
                    setResult(dataset.encodePrettily(), "application/json", dataInfo).forward()
                    log.info("Dataset imported: $dataInfo")
                    log.debug(dataset.encodePrettily())
                }
        }
    }

    companion object {
        const val ADDRESS: String = "io.piveau.pipe.importing.jsonapi.queue"
    }

}
