# ChangeLog

## Unreleased

## 1.1.2 (2023-10-04)

**Changed:**
* Lib dependencies
* CI/CD

## 1.1.1 (2021-06-05)

**Changed:**
* Lib dependencies

## 1.1.0 (2021-01-31)

**Changed:**
* Switched to Vert.x 4.0.0

## 1.0.1 (2020-09-09)

**Added:**
* Send identifier list lastly
* Warn when duplicate identifier

**Changed:**
* Use id AND type to construct the identifier

## 1.0.0 (2020-07-30)

Initial release
