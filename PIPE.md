# Pipe Segment Configuration

_mandatory_

* `address`

  Address of the source

* `catalogue`

  The id of the target catalogue

_optional_

* `sendListDelay`

  The delay in milliseconds before the list of identifiers is send. Take precedence over service configuration (see `PVEAU_IMPORTING_SEND_LIST_DELAY`)

# Data Info Object

* `total`

  Total number of datasets

* `counter`

  The number of this dataset

* `identifier`

  The unique identifier in the source of this dataset

* `catalogue`

  The id of the target catalogue

